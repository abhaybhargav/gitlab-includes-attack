const taxPayer = require("../models/taxPayers");
const libxml = require('libxmljs')
const fs = require('fs')
const xmljs = require('xml2js')

module.exports = async (req, res, next) => {
    taxPayer.findById(req.query.pan)
        .then(response => {
            if (!response) {
                res.status(400).json({
                    error: "no such user exists"
                })
            } else {
                const x = response.data
                const xmlObject = Buffer.from(x, 'base64').toString()

                const xmlDoc = libxml.parseXml(xmlObject)

                const info = {
                    "tax-filing": {
                        "name": xmlDoc.get('//tax-filing').get('//name').text(),
                        "email": xmlDoc.get('//tax-filing').get('//email').text(),
                        "pan": xmlDoc.get('//tax-filing').get('//pan').text(),
                        "investments": {
                            "ppf": xmlDoc.get('//tax-filing').get('//investments').get('//ppf').text(),
                            "mutual-funds": xmlDoc.get('//tax-filing').get('//investments').get('//mutual-funds').text(),
                            "fixed-deposit": xmlDoc.get('//tax-filing').get('//investments').get('//fixed-deposit').text(),
                            "home-loan-principal": xmlDoc.get('//tax-filing').get('//investments').get('//home-loan-principal').text(),
                            "insurance": xmlDoc.get('//tax-filing').get('//investments').get('//insurance').text(),
                            "medical-insurance": xmlDoc.get('//tax-filing').get('//investments').get('//medical-insurance').text()
                        }
                    }

                }


                const builder = new xmljs.Builder()
                const xmlResponse = builder.buildObject(info)

                fs.writeFileSync(`${__dirname}\\${req.query.pan}.xml`, xmlResponse)

                res.download(`${__dirname}\\${req.query.pan}.xml`, err => {
                    if (!err) {
                        fs.unlinkSync(`${__dirname}\\${req.query.pan}.xml`)
                    }
                })
            }
        })
        .catch(err => {
            res.status(500).json({
                error: err
            });
        })
};
