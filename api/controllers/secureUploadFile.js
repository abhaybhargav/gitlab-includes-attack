const taxPayer = require('../models/taxPayers')
const libxml = require('libxmljs')
const xmljs = require('xml2js')

module.exports = (req, res, next) => {
    const result = req.files.file.data.toString()
    const xmlDoc = libxml.parseXml(result)

    const info = {
        "tax-filing": {
            "name": xmlDoc.get('//tax-filing').get('//name').text(),
            "email": xmlDoc.get('//tax-filing').get('//email').text(),
            "pan": xmlDoc.get('//tax-filing').get('//pan').text(),
            "investments": {
                "ppf": xmlDoc.get('//tax-filing').get('//investments').get('//ppf').text(),
                "mutual-funds": xmlDoc.get('//tax-filing').get('//investments').get('//mutual-funds').text(),
                "fixed-deposit": xmlDoc.get('//tax-filing').get('//investments').get('//fixed-deposit').text(),
                "home-loan-principal": xmlDoc.get('//tax-filing').get('//investments').get('//home-loan-principal').text(),
                "insurance": xmlDoc.get('//tax-filing').get('//investments').get('//insurance').text(),
                "medical-insurance": xmlDoc.get('//tax-filing').get('//investments').get('//medical-insurance').text()
            }
        }

    }

    
        const builder = new xmljs.Builder()
        const xmlResponse = builder.buildObject(info)
        const data = Buffer.from(xmlResponse).toString('base64')
    

    const pan = xmlDoc.get('//tax-filing').get('//pan').text()


    taxPayer.findByIdAndUpdate(pan, { data: data })
        .then(response => {
            if (response) {
                res.status(201).json({
                    msg: 'Data Updated Successfully'
                })
            } else {
                res.status(200).json({
                    msg: "Please enter a valid PAN"
                })
            }
        })
        .catch(err => {
            res.status(500).send()
        })

}