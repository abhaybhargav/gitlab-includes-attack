const express = require('express')
const router = express.Router()
const newTaxPayer = require('../controllers/newTaxPayer')
const getTaxPayer = require('../controllers/getTaxPayer')
const updateTaxPayer = require('../controllers/updateTaxPayer')
const secureGetTaxPayer = require('../controllers/secureGetTaxPayer')

router.route('/')
    .post(newTaxPayer)
    .get(getTaxPayer)
    .put(updateTaxPayer)
    
router.route('/secure')
    .get(secureGetTaxPayer)

module.exports = router