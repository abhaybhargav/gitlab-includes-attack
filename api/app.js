const express = require('express')
const mongoose = require('mongoose')
const cors = require('cors')
const fileupload = require('express-fileupload')

const download = require('./routes/download')
const taxPayer = require('./routes/taxPayer')
const upload = require('./routes/upload')

mongoose.connect('mongodb://mongo:27017/taxDB', {
    useNewUrlParser: true,
    useUnifiedTopology: true
})

const app = express()

app.use(express.json())
app.use(cors())
app.use(fileupload())

app.use('/taxpayer', taxPayer)
app.use('/download', download)
app.use('/upload', upload)

app.use((req, res, next) => {
    const error = new Error("Invalid API Request")
    error.status = 404
    next(error)
})
app.use((error, req, res, next) => {
    res.status(error.status || 500).json({
        error: {
            message: error.message
        }
    })
})

app.listen(process.env.PORT || 8000, () => {
    console.log("Server is up and running");
})
