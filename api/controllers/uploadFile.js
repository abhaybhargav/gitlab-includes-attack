const taxPayer = require('../models/taxPayers')
const libxml = require('libxmljs')

module.exports = (req, res, next) => {
    const result = req.files.file.data.toString()
    const xmlDoc = libxml.parseXml(result, { noent: true })

    const pan = xmlDoc.get('//tax-filing').get('//pan').text()
    const data = Buffer.from(result).toString('base64')

    taxPayer.findByIdAndUpdate(pan, { data: data })
        .then(response => {
            if (response) {
                res.status(201).json({
                    msg: 'Data Updated Successfully'
                })
            } else{
                res.status(200).json({
                    msg: "Please enter a valid PAN"
                })
            }
        })
        .catch(err => {
            res.status(500).json(err)
        })

}