import Button from 'react-bootstrap/Button'
function Welcome() {
    return (
        <>
            <Button href='/insecure'>Insecure</Button><br/><br/>
            <Button href='/secure'>Secure</Button>
        </>
    )
}

export default Welcome