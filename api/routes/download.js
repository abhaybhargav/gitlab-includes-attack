const express = require('express')
const router = express.Router();
const downloadReport = require('../controllers/downloadReport')
const secureDownloadReport = require('../controllers/secureDownloadReport')

router.route('/')
    .get(downloadReport)

router.route('/secure')
    .get(secureDownloadReport)

module.exports = router